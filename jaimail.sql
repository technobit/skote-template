/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.4.10-MariaDB : Database - jaimail
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`jaimail` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `jaimail`;

/*Table structure for table `h_import` */

DROP TABLE IF EXISTS `h_import`;

CREATE TABLE `h_import` (
  `h_id` int(16) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `import_date` datetime DEFAULT current_timestamp(),
  `file_name` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `direktori` text COLLATE latin1_general_ci DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `is_replace` tinyint(1) DEFAULT 1,
  `var_subjek_email` varchar(1000) COLLATE latin1_general_ci DEFAULT NULL,
  `txt_isi_email` text COLLATE latin1_general_ci DEFAULT NULL,
  `dt_gaji_dikirim` date DEFAULT NULL,
  `created_by` int(5) DEFAULT NULL,
  `updated_by` int(5) DEFAULT NULL,
  `int_level` int(5) DEFAULT NULL,
  PRIMARY KEY (`h_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `h_import` */

/*Table structure for table `m_email_config` */

DROP TABLE IF EXISTS `m_email_config`;

CREATE TABLE `m_email_config` (
  `int_config_id` int(11) NOT NULL AUTO_INCREMENT,
  `var_nama_config` varchar(100) NOT NULL,
  `var_host` varchar(100) NOT NULL,
  `var_email` varchar(100) NOT NULL,
  `var_pass` varchar(100) NOT NULL,
  `var_secure` varchar(50) NOT NULL,
  `int_port` int(11) NOT NULL,
  PRIMARY KEY (`int_config_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*Data for the table `m_email_config` */

insert  into `m_email_config`(`int_config_id`,`var_nama_config`,`var_host`,`var_email`,`var_pass`,`var_secure`,`int_port`) values 
(1,'JAI MAIL SERVER','smtp.gmail.com','serverbululawang@gmail.com','Root4bululawang','TLS',587);

/*Table structure for table `m_gaji_ref` */

DROP TABLE IF EXISTS `m_gaji_ref`;

CREATE TABLE `m_gaji_ref` (
  `int_ref_id` int(3) NOT NULL,
  `var_kode_kolom` varchar(50) NOT NULL,
  `var_nama_kolom` varchar(100) DEFAULT NULL,
  `is_edit` int(1) DEFAULT 1,
  PRIMARY KEY (`var_kode_kolom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `m_gaji_ref` */

insert  into `m_gaji_ref`(`int_ref_id`,`var_kode_kolom`,`var_nama_kolom`,`is_edit`) values 
(1,'a','NIK',0),
(27,'aa','Koreksi THR',1),
(28,'ab','HAT',1),
(29,'ac','Tagihan Medical',1),
(30,'ad','Pot. Absensi',1),
(31,'ae','Koreksi Gaji',1),
(32,'af','Rapel Gaji',1),
(33,'ag','Pesangon',1),
(34,'ah','Death Benefit',1),
(35,'ai','Tunj. Pajak',1),
(36,'aj','Insentif Lembur',1),
(37,'ak','Pendapatan Lain 1',1),
(38,'al','Pendapatan Lain 2',1),
(39,'am','Pendapatan Lain 3',1),
(40,'an','Pendapatan Lain 4',1),
(41,'ao','Pendapatan Lain 5',1),
(42,'ap','Pendapatan Bruto',0),
(43,'aq','PAJAK',0),
(44,'ar','Manulife',1),
(45,'as','Jamsostek',1),
(46,'at','Pot Masyitoh',1),
(47,'au','Pot Perusahaan',1),
(48,'av','Pengembalian BPJS',1),
(49,'aw','Pot SP',1),
(50,'ax','Jaminan Pensiun',1),
(51,'ay','Pot Koperasi',1),
(52,'az','Pot KOPPER',1),
(2,'b','NAMA',0),
(53,'ba','Pot RS Sido Waras',1),
(54,'bb','Pot RS Lain',1),
(55,'bc','Pot RS Siti Hajar',1),
(56,'bd','Pot RS Gatoel',1),
(57,'be','BJPS Keluarga',1),
(58,'bf','Iuran BPJS kes',1),
(59,'bg','Potongan lain 1',1),
(60,'bh','Potongan lain 2',1),
(61,'bi','Potongan lain 3',1),
(62,'bj','Potongan lain 4',1),
(63,'bk','Potongan lain 5',1),
(64,'bl','Potongan lain 6',1),
(65,'bm','Total Potongan',0),
(66,'bn','PENDAPATAN BERSIH',0),
(67,'bo','PDP',0),
(68,'bp','DIBAYARKAN',0),
(69,'bq','PEMBAYARAN',0),
(70,'br','NO REK',0),
(71,'bs','SISA PLAFON',0),
(72,'bt','SISA CUTI',0),
(73,'bu','PESAN',0),
(74,'bv','EMAIL',0),
(3,'c','LOKASI',0),
(4,'d','BAGIAN',0),
(5,'e','VALUTA',0),
(6,'f','Gaji Pokok',0),
(7,'g','Uang Saku',1),
(8,'h','Sisa Uang Cuti',1),
(9,'i','UPD',1),
(10,'j','Gapok Proposional',1),
(11,'k','Tunj. Shift Malam',1),
(12,'l','Lembur',1),
(13,'m','Lembur (jam)',1),
(14,'n','Tunj. Jabatan',1),
(15,'o','Transport',1),
(16,'p','Transport (jmlh hari)',1),
(17,'q','Premi Kehadiran',1),
(18,'r','Koreksi Cuti Besar',1),
(19,'s','VP Susulan',1),
(20,'t','Uang Makan',1),
(21,'u','Uang Cuti besar',1),
(22,'v','Medical Allowance',1),
(23,'w','Biaya pernikahan',1),
(24,'x','Biaya Kacamata',1),
(25,'y','Biaya Kematian',1),
(26,'z','Biaya Kelahiran',1);

/*Table structure for table `m_template_email` */

DROP TABLE IF EXISTS `m_template_email`;

CREATE TABLE `m_template_email` (
  `int_template_id` int(11) NOT NULL AUTO_INCREMENT,
  `var_subjek_email` varchar(1000) DEFAULT NULL,
  `txt_isi_email` text DEFAULT NULL,
  PRIMARY KEY (`int_template_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `m_template_email` */

insert  into `m_template_email`(`int_template_id`,`var_subjek_email`,`txt_isi_email`) values 
(1,'SLIP GAJI KARYAWAN PT JAI','Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur');

/*Table structure for table `m_user_admin` */

DROP TABLE IF EXISTS `m_user_admin`;

CREATE TABLE `m_user_admin` (
  `int_id_user` int(11) NOT NULL AUTO_INCREMENT,
  `txt_nama_depan` varchar(100) DEFAULT NULL,
  `txt_nama_belakang` varchar(100) DEFAULT NULL,
  `txt_username` varchar(50) DEFAULT NULL,
  `txt_password` varchar(255) DEFAULT NULL,
  `int_id_group` int(11) DEFAULT NULL,
  `int_status` int(11) DEFAULT NULL,
  `int_level` int(1) DEFAULT NULL,
  `dt_registrasi` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`int_id_user`) USING BTREE,
  KEY `intIDuser` (`int_id_user`) USING BTREE,
  KEY `IDX_m_user_admin_intIDGroup` (`int_id_group`) USING BTREE,
  FULLTEXT KEY `IDX_m_user_admin_txtUsername` (`txt_username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

/*Data for the table `m_user_admin` */

insert  into `m_user_admin`(`int_id_user`,`txt_nama_depan`,`txt_nama_belakang`,`txt_username`,`txt_password`,`int_id_group`,`int_status`,`int_level`,`dt_registrasi`) values 
(1,'Super','Admin','super','$2y$10$qTT3fCQD2.R7PwkKpKvHEusSz.nTQyEjnGjePsY3MQiXDM2XB90cq',1,1,1,'2019-11-11 20:40:54'),
(2,'golongan','satu-tiga','gol13','$2y$10$nkUu2Ep.iqRG70iv1evSGeUH9kv5iU96N0hqQ8JA8tdlUHk26v.Na',1,1,2,'2020-10-13 10:49:58');

/*Table structure for table `m_user_admin_group` */

DROP TABLE IF EXISTS `m_user_admin_group`;

CREATE TABLE `m_user_admin_group` (
  `int_id_group` int(11) NOT NULL AUTO_INCREMENT,
  `txt_nama` varchar(50) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  PRIMARY KEY (`int_id_group`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `m_user_admin_group` */

insert  into `m_user_admin_group`(`int_id_group`,`txt_nama`,`is_active`) values 
(1,'Admin Sistem',1);

/*Table structure for table `m_user_admin_group_menu` */

DROP TABLE IF EXISTS `m_user_admin_group_menu`;

CREATE TABLE `m_user_admin_group_menu` (
  `int_id` int(11) NOT NULL AUTO_INCREMENT,
  `int_id_group` int(11) DEFAULT NULL,
  `int_id_menu` int(11) DEFAULT NULL,
  `c` tinyint(4) DEFAULT 0,
  `r` tinyint(4) DEFAULT 0,
  `u` tinyint(4) DEFAULT 0,
  `d` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`int_id`) USING BTREE,
  KEY `IDX_m_user_admin_group_menu_intIDGroup` (`int_id_group`) USING BTREE,
  KEY `IDX_m_user_admin_group_menu_intIDMenu` (`int_id_menu`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `m_user_admin_group_menu` */

insert  into `m_user_admin_group_menu`(`int_id`,`int_id_group`,`int_id_menu`,`c`,`r`,`u`,`d`) values 
(24,1,1,1,1,1,1),
(25,1,2,1,1,1,1),
(26,1,9,0,1,0,0),
(27,1,91,1,1,1,1),
(28,1,92,1,1,1,1),
(29,1,93,1,1,1,1),
(30,1,94,1,1,1,1),
(31,1,95,1,1,1,1);

/*Table structure for table `m_user_admin_menu` */

DROP TABLE IF EXISTS `m_user_admin_menu`;

CREATE TABLE `m_user_admin_menu` (
  `int_id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `txt_kode` varchar(25) DEFAULT NULL,
  `txt_nama` varchar(50) DEFAULT NULL,
  `txt_url` varchar(255) DEFAULT NULL,
  `int_level` int(11) DEFAULT NULL,
  `int_urutan` int(11) DEFAULT NULL,
  `int_parent_id` int(11) DEFAULT NULL,
  `txt_class` varchar(25) DEFAULT NULL,
  `txt_icon` varchar(50) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`int_id_menu`) USING BTREE,
  KEY `IDX_m_user_admin_menu_intIdMenu` (`int_id_menu`) USING BTREE,
  KEY `IDX_m_user_admin_menu_intParentId` (`int_parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=425 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `m_user_admin_menu` */

insert  into `m_user_admin_menu`(`int_id_menu`,`txt_kode`,`txt_nama`,`txt_url`,`int_level`,`int_urutan`,`int_parent_id`,`txt_class`,`txt_icon`,`is_active`) values 
(1,'DASHBOARD','Dashboard','/',1,1,NULL,'dashboard','fas fa-th-large',1),
(2,'GAJI','Penggajian','gaji',1,2,NULL,'gaji','fas fa-money-bill-wave-alt',1),
(9,'SYSTEM','Konfigurasi Sistem',NULL,1,9,NULL,'sistem','fas fa-cogs',1),
(91,'EXCEL','Kolom Excel','s_excel',2,91,9,'s_excel','fas fa-file-excel',1),
(92,'EMAIL','Konfigurasi Email','s_email',2,92,9,'s_email','fas fa-envelope',1),
(93,'USER-ADMIN','Administrator','s_user',2,93,9,'s_user','fas fa-users-cog',1),
(94,'MENU','Daftar Menu','s_menu',2,94,9,'s_menu','fas fa-th-list',1),
(95,'GROUP','Grup Menu','s_group',2,95,9,'s_group','fas fa-users',1);

/*Table structure for table `t_gaji` */

DROP TABLE IF EXISTS `t_gaji`;

CREATE TABLE `t_gaji` (
  `int_gaji_id` int(11) NOT NULL AUTO_INCREMENT,
  `dt_gaji_dikirim` date NOT NULL,
  `a` varchar(50) DEFAULT NULL,
  `b` varchar(100) DEFAULT NULL,
  `c` varchar(50) DEFAULT NULL,
  `d` varchar(50) DEFAULT NULL,
  `e` varchar(50) DEFAULT NULL,
  `f` decimal(15,0) DEFAULT 0,
  `g` decimal(15,0) DEFAULT 0,
  `h` decimal(15,0) DEFAULT 0,
  `i` decimal(15,0) DEFAULT 0,
  `j` decimal(15,0) DEFAULT 0,
  `k` decimal(15,0) DEFAULT 0,
  `l` decimal(15,0) DEFAULT 0,
  `m` decimal(15,0) DEFAULT 0,
  `n` decimal(15,0) DEFAULT 0,
  `o` decimal(15,0) DEFAULT 0,
  `p` int(11) DEFAULT 0,
  `q` decimal(15,0) DEFAULT 0,
  `r` decimal(15,0) DEFAULT 0,
  `s` decimal(15,0) DEFAULT 0,
  `t` decimal(15,0) DEFAULT 0,
  `u` decimal(15,0) DEFAULT 0,
  `v` decimal(15,0) DEFAULT 0,
  `w` decimal(15,0) DEFAULT 0,
  `x` decimal(15,0) DEFAULT 0,
  `y` decimal(15,0) DEFAULT 0,
  `z` decimal(15,0) DEFAULT 0,
  `aa` decimal(15,0) DEFAULT 0,
  `ab` decimal(15,0) DEFAULT 0,
  `ac` decimal(15,0) DEFAULT 0,
  `ad` decimal(15,0) DEFAULT 0,
  `ae` decimal(15,0) DEFAULT 0,
  `af` decimal(15,0) DEFAULT 0,
  `ag` decimal(15,0) DEFAULT 0,
  `ah` decimal(15,0) DEFAULT 0,
  `ai` decimal(15,0) DEFAULT 0,
  `aj` decimal(15,0) DEFAULT 0,
  `ak` decimal(15,0) DEFAULT 0,
  `al` decimal(15,0) DEFAULT 0,
  `am` decimal(15,0) DEFAULT 0,
  `an` decimal(15,0) DEFAULT 0,
  `ao` decimal(15,0) DEFAULT 0,
  `ap` decimal(15,0) DEFAULT 0,
  `aq` decimal(15,0) DEFAULT 0,
  `ar` decimal(15,0) DEFAULT 0,
  `as` decimal(15,0) DEFAULT NULL,
  `at` decimal(15,0) DEFAULT 0,
  `au` decimal(15,0) DEFAULT 0,
  `av` decimal(15,0) DEFAULT 0,
  `aw` decimal(15,0) DEFAULT 0,
  `ax` decimal(15,0) DEFAULT 0,
  `ay` decimal(15,0) DEFAULT 0,
  `az` decimal(15,0) DEFAULT 0,
  `ba` decimal(15,0) DEFAULT 0,
  `bb` decimal(15,0) DEFAULT 0,
  `bc` decimal(15,0) DEFAULT 0,
  `bd` decimal(15,0) DEFAULT 0,
  `be` decimal(15,0) DEFAULT 0,
  `bf` decimal(15,0) DEFAULT 0,
  `bg` decimal(15,0) DEFAULT 0,
  `bh` decimal(15,0) DEFAULT 0,
  `bi` decimal(15,0) DEFAULT 0,
  `bj` decimal(15,0) DEFAULT 0,
  `bk` decimal(15,0) DEFAULT 0,
  `bl` decimal(15,0) DEFAULT 0,
  `bm` decimal(15,0) DEFAULT 0,
  `bn` decimal(15,0) DEFAULT 0,
  `bo` decimal(15,0) DEFAULT 0,
  `bp` decimal(15,0) DEFAULT 0,
  `bq` varchar(50) DEFAULT NULL,
  `br` varchar(50) DEFAULT NULL,
  `bs` varchar(255) DEFAULT NULL,
  `bt` int(11) DEFAULT 0,
  `bu` text DEFAULT NULL,
  `bv` varchar(100) DEFAULT NULL,
  `int_status` int(11) DEFAULT 0 COMMENT '1 = Belum Dikirim\r\n2 = Proses\r\n3 = Terkirim\r\n4 = Gagal Dikirim',
  `dt_periode` date DEFAULT NULL,
  `id_import` int(11) DEFAULT NULL,
  PRIMARY KEY (`int_gaji_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `t_gaji` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
