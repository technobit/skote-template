<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends MX_Controller {
    private $mode = ['Hari Ini', 'Minggu Ini', 'Bulan Ini'];
	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'APP-HOME';
		$this->authCheck();
		
		$this->load->model('app_model', 'model');
    }
	
	public function index(){
		$this->page->subtitle = 'Dashboard';
		$this->page->menu = 'dashboard';
        $this->breadcrumb->title = 'Dashboard';;
        $this->breadcrumb->list = [getServerDate(true)];
        $this->js = true;
        
        $last_import = $this->model->get_last_import();
        if($last_import){
            $last_success = $this->model->get_last_data($last_import->h_id, 3);
            $last_progress = $this->model->get_last_data($last_import->h_id, 2);
            $last_failed = $this->model->get_last_data($last_import->h_id, 4);

            $data['last_import']  = $last_import->total;
            $data['last_success']  = $last_success->total;
            $data['last_progress']  = $last_progress->total;
            $data['last_failed']  = $last_failed->total;
        }else{
            $data['last_import']  = 0;
            $data['last_success']  = 0;
            $data['last_progress']  = 0;
            $data['last_failed']  = 0;
        }

		$this->render_view('index', $data, false);
	}
}
