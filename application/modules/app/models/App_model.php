<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class App_model extends MY_Model {

    public function get_last_data($id_import, $int_status){
	    return $this->db->query("SELECT COUNT(*) as total FROM {$this->t_gaji}
        WHERE int_status = $int_status AND id_import = $id_import
        ")->row();
    }

    public function get_last_import(){
        $level = $this->session->userdata['int_level'];

        if($level == 2){
            $where =  "WHERE int_level = {$level}";
        }else{
            $where = "";
        }
	    return $this->db->query("SELECT * FROM {$this->h_import}
        {$where}
        ORDER BY import_date DESC LIMIT 1")->row();
    }
}