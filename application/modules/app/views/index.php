    <!-- <div class="card card-outline card-danger">

         <div class="card-body">
            <div class="clearfix">

                <h4 class="card-title mb-4"> <i class="fas fa-calendar-alt"></i>
                    Rekapitulasi Penggajian Terakhir</h4>
            </div> -->
            <div class="row">
                <div class="col-lg-3 col-6">
                    <div class="mini-stats-wid card">
                        <div class="card-body">
                            <div class="d-flex flex-wrap">
                                <div class="me-3">
                                    <p class="text-muted mb-2">Data Gaji</p>
                                    <h5 class="mb-0"><?= $last_import ?></h5>
                                </div>
                                <div class="avatar-sm ms-auto">
                                    <div class="avatar-title bg-light rounded-circle text-primary font-size-20"><i class="fas fa-receipt"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="mini-stats-wid card">
                        <div class="card-body">
                            <div class="d-flex flex-wrap">
                                <div class="me-3">
                                    <p class="text-muted mb-2">Email Terkirim</p>
                                    <h5 class="mb-0"><?= $last_success ?></h5>
                                </div>
                                <div class="avatar-sm ms-auto">
                                    <div class="avatar-title bg-light rounded-circle text-primary font-size-20"><i class="fas fa-check-double"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              
                <div class="col-lg-3 col-6">
                    <div class="mini-stats-wid card">
                        <div class="card-body">
                            <div class="d-flex flex-wrap">
                                <div class="me-3">
                                    <p class="text-muted mb-2">Email Dalam Proses</p>
                                    <h5 class="mb-0"><?= $last_progress ?></h5>
                                </div>
                                <div class="avatar-sm ms-auto">
                                    <div class="avatar-title bg-light rounded-circle text-primary font-size-20"><i class="fas fa-hourglass-half"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="mini-stats-wid card">
                        <div class="card-body">
                            <div class="d-flex flex-wrap">
                                <div class="me-3">
                                    <p class="text-muted mb-2">Email Gagal Terkirim</p>
                                    <h5 class="mb-0"><?= $last_failed ?></h5>
                                </div>
                                <div class="avatar-sm ms-auto">
                                    <div class="avatar-title bg-light rounded-circle text-primary font-size-20"><i class="fas fa-exclamation-triangle"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
        <!-- </div> -->
        <div id="ajax-modal" class="modal animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>