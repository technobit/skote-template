<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
	
class Detail extends MX_Controller {

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'GAJI';
		$this->module   = 'penggajian';
		$this->routeURL = 'detail';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('Detail_model', 'model');

    }
	
	public function index($id_import){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Daftar Penggajian';
		$this->page->menu 	  = 'gaji';
		//$this->page->submenu1 = 'penggajian';
		$this->breadcrumb->title = 'Daftar Pengajian';
		$this->breadcrumb->card_title = 'Daftar Data Penggajian';
		$this->breadcrumb->icon = 'fas fa-money-bill-wave-alt';
		$this->breadcrumb->list = ['Penggajian', 'Daftar Penggajian'];
		$this->js = true;
		$data['detail'] = $this->model->get_import($id_import);
		$data['list_detail_url'] = site_url("{$this->routeURL}/{$id_import}");
		$data['url'] = site_url("{$this->routeURL}/add");
		$data['url_resend_all'] = site_url("{$this->routeURL}/resendall/{$id_import}");
		$data['url_back'] = site_url("gaji");
		$data['url_export'] = site_url("export/{$this->routeURL}");
		$this->render_view('detail/index', $data, true);
	}

	public function list($id_import){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$totald = $this->model->listCount($id_import, $this->input->post('status_filter', true), $this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($id_import, $this->input->post('status_filter', true), $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i		= $this->input_post('start', true);
		foreach($ldata as $dt){
			$action = '<a href="'.site_url("{$this->routeURL}/").$dt->int_gaji_id.'/slip" class="btn btn-sm btn-primary tooltips"><i class="fas fa-receipt"></i></a>';
			if($dt->int_status == 4){
				$action .= '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$dt->int_gaji_id.'/resend" class="ajax_modal btn btn-sm btn-warning tooltips" data-placement="top" data-original-title="Resend" style="margin-left:5px"><i class="fas fa-sync-alt"></i></a>';
			}
			$i++;
			$data[] = array($i.'. ', $dt->a, $dt->b, $dt->d, $dt->bv, idn_date($dt->dt_gaji_dikirim), $dt->int_status, $action);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $totald,
								'iTotalDisplayRecords' => $totald,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()),
							200, false);
	}

	public function confirm($int_gaji_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get_gaji($int_gaji_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$int_gaji_id/resend");
			$data['title']	= 'Kirim Ulang Slip Gaji';
			$data['info']   = [ 'NIK' => $res->a,
                                'Nama' => $res->b,
								'Lokasi' => $res->c,
								'Bagian' => $res->d,
								'Tanggal Kirim' => idn_date($res->dt_gaji_dikirim)
							];
			$this->load_view('detail/resend_confirm', $data);
		}
	}

	public function resend($int_gaji_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->resend($int_gaji_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Email Resend Successfully" : "Email Failed to Resend",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}

	public function confirm_resendall($id_import){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get_import($id_import);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/resendall/{$id_import}");
			$data['title']	= 'Kirim Ulang Semua yang Belum Terkirim?';
			$data['info']   = [ 'Tanggal Upload' => idn_date($res->import_date, "j F Y H:i:s"),
                                'Periode' => idn_date($res->dt_periode, "F Y"),
                                'Jadwal Kirim' => idn_date($res->dt_gaji_dikirim)];
			$this->load_view('detail/resendall_confirm', $data);
		}
	}

	public function resendall($id_import){
		$this->authCheckDetailAccess('d');

		$check = $this->model->resendall($id_import);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Email Resend Successfully" : "Email Failed to Resend",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}

	public function get_gaji($int_gaji_id){
		if($this->authCheckDetailAccess('u', true) == false) return;
		$this->page->subtitle = 'Slip Gaji';
		$this->page->menu 	  = 'gaji';
		$this->breadcrumb->title = 'Slip Gaji';
		$this->breadcrumb->list = ['Penggajian', 'Slip Gaji'];

		$res = $this->model->get_gaji($int_gaji_id);
		$ref = $this->model->ref_gaji();
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'posts Not Found', 'message' => '']],true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$int_gaji_id");
			$data['title']	= 'Slip Gaji';

			$data['gaji']   = [ 'NIK' => $res->a,
			'Nama' => $res->b,
			'Lokasi' => $res->c,
			'Bagian' => $res->d,
			'Tanggal Kirim' => idn_date($res->dt_gaji_dikirim)];

			$pot = 4;
			$penerimaan = ['"'.$ref[5]->var_nama_kolom.'"' => $res->f,
					'"'.$ref[6]->var_nama_kolom.' ('.$res->ak.' Hari)"' => $res->g,
					'"'.$ref[7]->var_nama_kolom.'"' => $res->h,
					'"'.$ref[8]->var_nama_kolom.'"' => $res->i,
					'"'.$ref[9]->var_nama_kolom.'"' => $res->j,
					'"'.$ref[10]->var_nama_kolom.'"' => $res->k,
					'"'.$ref[11]->var_nama_kolom.' ('.number_format($res->m,2, ',', '.' ).' Jam)"' => $res->l,
					//'"'.$ref[12]->var_nama_kolom.'"' => $res->m,
					'"'.$ref[13]->var_nama_kolom.'"' => $res->n,
					'"'.$ref[14]->var_nama_kolom.' ('.$res->p.' Hari)"'  => $res->o,
					//'"'.$ref[15]->var_nama_kolom.'"' => $res->p,
					'"'.$ref[16]->var_nama_kolom.'"' => $res->q,
					'"'.$ref[17]->var_nama_kolom.'"' => $res->r,
					'"'.$ref[18]->var_nama_kolom.'"' => $res->s,
					'"'.$ref[19]->var_nama_kolom.'"' => $res->t,
					'"'.$ref[20]->var_nama_kolom.'"' => $res->u,
					'"'.$ref[21]->var_nama_kolom.'"' => $res->v,
					'"'.$ref[22]->var_nama_kolom.'"' => $res->w,
					'"'.$ref[23]->var_nama_kolom.'"' => $res->x,
					'"'.$ref[24]->var_nama_kolom.'"' => $res->y,
					'"'.$ref[25]->var_nama_kolom.'"' => $res->z,
					'"'.$ref[26]->var_nama_kolom.'"' => $res->aa,
					'"'.$ref[27]->var_nama_kolom.'"' => $res->ab,
					'"'.$ref[28]->var_nama_kolom.'"' => $res->ac,
					'"'.$ref[29]->var_nama_kolom.'"' => $res->ad,
					'"'.$ref[30]->var_nama_kolom.'"' => $res->ae,
					'"'.$ref[31]->var_nama_kolom.'"' => $res->af,
					'"'.$ref[32]->var_nama_kolom.'"' => $res->ag,
					'"'.$ref[33]->var_nama_kolom.'"' => $res->ah,
					'"'.$ref[34]->var_nama_kolom.'"' => $res->ai,
					'"'.$ref[35]->var_nama_kolom.'"' => $res->aj,
					//'"'.$ref[36]->var_nama_kolom.'"' => $res->ak,
					'"'.$ref[37]->var_nama_kolom.'"' => $res->al,
					'"'.$ref[38]->var_nama_kolom.'"' => $res->am,
					'"'.$ref[39]->var_nama_kolom.'"' => $res->an,
					'"'.$ref[40]->var_nama_kolom.'"' => $res->ao,
				];
			$potongan = ['"'.$ref[43]->var_nama_kolom.'"' => $res->ar,
					'"'.$ref[44]->var_nama_kolom.'"' => $res->as,
					'"'.$ref[45]->var_nama_kolom.'"' => $res->at,
					'"'.$ref[46]->var_nama_kolom.'"' => $res->au,
					'"'.$ref[47]->var_nama_kolom.'"' => $res->av,
					'"'.$ref[48]->var_nama_kolom.'"' => $res->aw,
					'"'.$ref[49]->var_nama_kolom.'"' => $res->ax,
					'"'.$ref[50]->var_nama_kolom.'"' => $res->ay,
					'"'.$ref[51]->var_nama_kolom.'"' => $res->az,
					'"'.$ref[52]->var_nama_kolom.'"' => $res->ba,
					'"'.$ref[53]->var_nama_kolom.'"' => $res->bb,
					'"'.$ref[54]->var_nama_kolom.'"' => $res->bc,
					'"'.$ref[55]->var_nama_kolom.'"' => $res->bd,
					'"'.$ref[56]->var_nama_kolom.'"' => $res->be,
					'"'.$ref[57]->var_nama_kolom.'"' => $res->bf,
					'"'.$ref[58]->var_nama_kolom.'"' => $res->bg,
					'"'.$ref[59]->var_nama_kolom.'"' => $res->bh,
					'"'.$ref[60]->var_nama_kolom.'"' => $res->bi,
					'"'.$ref[61]->var_nama_kolom.'"' => $res->bj,
					'"'.$ref[62]->var_nama_kolom.'"' => $res->bk,
					'"'.$ref[53]->var_nama_kolom.'"' => $res->bl
				];

			$data['penerimaan']	= $penerimaan;
			$data['potongan'] 	= $potongan;
			$data['url'] = site_url("{$this->routeURL}/{$res->id_import}");
			$data['data'] 	= $res;

		}
		$this->render_view('detail/slip_gaji', $data);
		
	}

	public function export($id_import){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page
		
		$per_tanggal = $this->input->post('tanggal_filter');

		$datagaji = $this->model->get_import($id_import);
		$ldata = $this->model->list($id_import, $this->input->post('status_filter', true));

        $title    = 'Data Penerima Gaji';

        $filename = 'Data Penerima Gaji - Periode '.idn_date($detail->dt_periode, "F Y");


		$input_file = 'assets/export/data_gaji.xlsx';
		/** Load $inputFileName to a Spreadsheet object **/
		//$spreadsheet = new Spreadsheet();
		$spreadsheet = PhpOffice\PhpSpreadsheet\IOFactory::load($input_file);
		$spreadsheet->setActiveSheetIndex(0)
					->setCellValue('A1', 'Daftar Penerima Gaji Periode '.idn_date($detail->dt_periode, "F Y"));

		$sheet = $spreadsheet->getActiveSheet();

        $i = 0;
        $x = 3;
        foreach($ldata as $d){
            $i++;
			$x++;

			switch ($d->int_status) {
				case 1:
					$status = 'Belum Dikirim';
					break;
				case 2:
					$status = 'Proses Pengiriman';
					break;
				case 3:
					$status = 'Belum Dikirim';
					break;
				case 4:
					$status = 'Terkirim';
					break;
				default:
					$status = 'Gagal Dikirim';
		  } 

            $sheet->setCellValue('A'.$x, $i);
			$sheet->setCellValueExplicit('B'.$x, $d->a,\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $sheet->setCellValue('C'.$x, $d->b);
            $sheet->setCellValue('D'.$x, $d->d);
            $sheet->setCellValue('E'.$x, $d->bv);
            $sheet->setCellValue('F'.$x, $status);
        }
		
		$sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		if (ob_get_contents()) ob_end_clean();
		$writer->save('php://output');
		exit;
	}
}
