<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="import-form" width="80%">
<div id="modal-import" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1">
				<label for="customFile" class="col-sm-4 col-form-label">File Import</label>
				
				<div class="col-sm-8">
					<div class="custom-file">
						<input type="file" class="form-control form-control-sm form-control-file" name="<?php echo $input_file_name?>" id="customFile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>
						<!-- <label class="custom-file-label" for="customFile">Pilih File</label> -->
					</div>
				</div>
			</div>
			<div class="form-group row mb-3">
				<label for="mulai" class="col-sm-4 col-form-label">Mulai Baris</label>
				<div class="col-sm-2">
					<input type="text" id="mulai" name="mulai" class="form-control form-control-sm currency text-right" value="2">
				</div>
				<i class="col-sm-6 required">*Baris data pada file excel</i>
			</div>
			<div class="form-group row mb-1 mt-3">
				<label for="dt_periode" class="col-sm-4 col-form-label">Periode Penggajian</label>
				<div class="col-sm-8">
					<input type="text" id="dt_periode" name="dt_periode" class="form-control form-control-sm date_picker text-right" value="<?=isset($data->dt_periode)? $data->dt_periode : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="dt_gaji_dikirim" class="col-sm-4 col-form-label">Tanggal Kirim</label>
				<div class="col-sm-8">
					<input type="text" id="dt_gaji_dikirim" name="dt_gaji_dikirim" class="form-control form-control-sm date_picker text-right" value="<?=isset($data->dt_gaji_dikirim)? $data->dt_gaji_dikirim : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_subjek_email" class="col-sm-4 col-form-label">Subjek eMail</label>
				<div class="col-sm-8">
					<input type="text" id="var_subjek_email" name="var_subjek_email" class="form-control form-control-sm" value="<?=isset($data->var_subjek_email)? $data->var_subjek_email : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="txt_isi_email" class="col-sm-4 col-form-label">Isi eMail</label>
				<div class="col-sm-8">
					<textarea class="form-control form-control-sm textarea" name="txt_isi_email" id="summernote" placeholder="isi email ..."><?=isset($data->txt_isi_email)? $data->txt_isi_email : ''?></textarea>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-bs-dismiss="modal" class="btn btn-danger">Batal</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$('.date_picker').daterangepicker(datepickModal);
		bsCustomFileInput.init();
		$('.currency').inputmask("numeric", {
            autoUnmask: true,
            radixPoint: ",",
            groupSeparator: ".",
            digits: 2,
            autoGroup: true,
            prefix: '',
            rightAlign: true,
            positionCaretOnClick: true,
        });
		$('#summernote').summernote({
			dialogsInBody: true,
			height: 250,                 // set editor height
			minHeight: null,             // set minimum height of editor
			maxHeight: null,             // set maximum height of editor
			focus: true                  // set focus to editable area after initializing summernote
		});
		$("#import-form").validate({
			rules: {
				<?php echo $input_file_name?>: {
					required: true,
					accept: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel",
				},
				mulai:{
			        required: true,
					digits:true
				},
				dt_gaji_dikirim:{
			        required: true
				},
				var_subjek_email:{
			        required: true,
					minlength: 5
				},
				txt_isi_email:{
			        required: true,
					minlength: 10
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
				blockUI('#modal-import', 'progress', 4);
                //let blc = '#modal-import';
                //blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
						setFormMessage('.form-message', data);
						if(data.stat){
							dataTable.draw();
							resetForm(form)
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>