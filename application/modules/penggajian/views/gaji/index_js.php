<script src="<?=base_url()?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/summernote/summernote-bs4.min.js"></script>

<script>
    var dataTable;
    $(document).ready(function() {
        dataTable = $('#table_gaji').DataTable({
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "pageLength": 50,
            "lengthMenu": [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "50",
                    "sClass": "text-right",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                    "sClass": "text-right",
                },
                {
                    "sWidth": "auto",
                },
                {
                    "sWidth": "auto",
                },
                {
                    "sWidth": "auto",
                },
				{
                    "sWidth": "120",
                    "bSearchable": false,
                    "bSortable": false
                }
            ],
            "aoColumnDefs": [{
                    "aTargets": [7],
                    "mRender": function(data, type, row, meta) {
                        return  ('<a href="<?=site_url("detail/") ?>' + data + '" class="btn btn-sm btn-primary tooltips" data-original-title="View" style="margin:20px 10px 20px 0"><i class="fa fa-th-list"></i></a> ') +
                                ('<a href="#" data-block="body" data-url="<?=site_url("{$routeURL}/") ?>' + data + '" class="ajax_modal btn btn-sm btn-warning tooltips" data-placement="top" data-original-title="Edit" ><i class="fa fa-edit"></i></a> ') +
                                ('<a href="#" data-block="body" data-url="<?=site_url("{$routeURL}/") ?>' + data + '/del" class="ajax_modal btn btn-sm btn-danger tooltips" data-placement="top" data-original-title="Hapus" ><i class="fa fa-trash"></i></a> ');
                    }
                }
            ]
        });
        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
				dataTable.search($(this).val()).draw();
			}
        });
    });
</script>