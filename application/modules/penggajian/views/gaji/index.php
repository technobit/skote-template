
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-outline">
               
                <div class="card-body">
                <div class="clearfix">
                    <div class="float-end">
                    <button type="button" data-block="body" class="btn btn-sm btn-success ajax_modal" data-url="<?=$url ?>" ><i class="fas fa-file-excel"></i> Import Data</button>
                  </div>
                    <h4 class="card-title mb-4">
                    <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
                </div>
                    <table class="table table-striped table-hover table-full-width" id="table_gaji">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Waktu Upload</th>
                            <th>Nama File</th>
                            <th>Total Data</th>
                            <th>Kirim eMail</th>
                            <th>Dibuat Oleh</th>
                            <th>Diubah Oleh</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div>

<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
<div id="ajax-modal-confirm" class="modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="50%" aria-hidden="true"></div>
