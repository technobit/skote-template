<form method="post" action="<?=$url?>" role="form" class="form-horizontal" id="action-confirm" width="80%">
<div id="modal-action" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
        <div class="modal-body p-0">
            <div class="mb-0 form-message text-center"></div>
            <div class="alert alert-warning mb-0">
                Apakah anda yakin mengirim ulang slip gaji berikut:
                <section class="landing">
                    <div class="container">
                        <dl class="row mb-0">
                            <?php foreach($info as $k => $v):?>
                                <dt class="col-sm-3 text-right"><strong><?=$k?>:</strong></dt><dd class="col-sm-9 mb-0"><?=$v ?></dd>
                            <?php endforeach;?>
                        </dl>
                    </div>
                </section>
            </div>
        </div>
		<div class="modal-footer">
			<button type="button" data-bs-dismiss="modal" class="btn btn-warning">Batal</button>
			<button type="submit" class="btn btn-success">Kirim</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$("#action-confirm").submit(function(){
            $('.form-message').html('');
			let blc = '#modal-action';
            blockUI(blc);
            $(this).ajaxSubmit({
                dataType:  'json',
                data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
                success: function(data){
                    refreshToken(data);
                    unblockUI(blc);
					setFormMessage('.form-message', data);
                    if(data.stat){
                        dataTable.draw();
                    }
                    closeModal($modal, data);
                }
            });
            return false;
		});
	});
</script>