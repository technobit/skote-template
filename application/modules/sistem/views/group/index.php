<div class="row">
	<section class="col-lg-12">
		<div class="card card-outline">

			<div class="card-body">
				<div class="clearfix">
					<div class="float-end">
						<button type="button" data-block="body" class="btn btn-xs btn-primary ajax_modal" data-url="<?= $url ?>"><i class="fas fa-plus"></i> Add</button>
					</div>
					<h4 class="card-title mb-4"> <i class="<?= isset($breadcrumb->icon) ? $breadcrumb->icon : 'far fa-circle' ?>"></i>
						<?= $breadcrumb->title ?></h4>
				</div>
				<div class="tab-content">
					<table class="table table-striped table-hover table-full-width" id="table_group">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama</th>
								<th>Status</th>
								<th></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>

<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>