<div class="row">
    <div class="col-md-6">
        <div class="card card-outline">

            <div class="card-body">
                <div class="clearfix">
                    <h4 class="card-title mb-4">
                        <i class="<?= isset($breadcrumb->icon) ? $breadcrumb->icon : 'far fa-circle' ?>"></i>
                        <?= isset($breadcrumb->card_title) ? $breadcrumb->card_title :  $breadcrumb->title ?>
                    </h4>
                </div>
                <div class="tab-content">
                    <?php
                    echo form_open($url_update, ['method' => "POST", "role" => "form", "class" => "form-horizontal", "id" => "email-form"]);
                    $msg = $this->session->flashdata('msg');
                    if (!empty($msg)) {
                        echo '<div class="alert alert-' . (($this->session->flashdata('stat')) ? 'success' : 'danger') . '">' . $msg . '</div>';
                    }
                    ?>
                    <div class="form-group row mb-1">
                        <label for="var_nama_config" class="col-sm-3 col-form-label">Nama Server</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control form-control-sm" id="var_nama_config" name="var_nama_config" value="<?php echo $data->var_nama_config ?>" />
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="var_host" class="col-sm-3 col-form-label">Host</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control form-control-sm" id="var_host" name="var_host" value="<?php echo $data->var_host ?>" />
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="var_email" class="col-sm-3 col-form-label">Email</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control form-control-sm" id="var_email" name="var_email" value="<?php echo $data->var_email ?>" />
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="var_pass" class="col-sm-3 col-form-label">Password</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control form-control-sm" id="var_pass" name="var_pass" value="<?php echo $data->var_pass ?>" />
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="var_secure" class="col-sm-3 col-form-label">Security</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control form-control-sm" id="var_secure" name="var_secure" value="<?php echo $data->var_secure ?>" />
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="int_port" class="col-sm-3 col-form-label">Port</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control form-control-sm  currency text-right" id="int_port" name="int_port" value="<?php echo $data->int_port ?>" />
                        </div>
                    </div>
                    <input type="hidden" id="int_config_id " name="int_config_id" value="<?php echo $data->int_config_id ?>" />
                </div>
                <div class="form-group row mb-1">
                    <div class="offset-sm-3 col-sm-9">
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>