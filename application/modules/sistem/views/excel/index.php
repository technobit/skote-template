<div class="row">
	<section class="col-lg-12">
		<div class="card card-outline">

			<div class="card-body">
				<div class="clearfix">
					<div class="float-end">
						<button type="button" class="btn btn-sm btn-success" onclick="exportData(this)" data-block="body" data-url="<?php echo $url_export ?>"><i class="fas fa-download"></i> Export Excel <i class="fas fa-file-excel"></i></button>
					</div>
					<h4 class="card-title mb-4"> <i class="<?= isset($breadcrumb->icon) ? $breadcrumb->icon : 'far fa-circle' ?>"></i>
						Konfigurasi Excel</h4>
				</div>
				<div class="tab-content">
				<table class="table table-striped table-hover table-full-width" id="table_data">
					<thead>
						<tr>
							<th>No</th>
							<th>Kolom</th>
							<th>Nama</th>
							<th></th>
						</tr>
					</thead>
				</table>
			</div>
			</div>
		</div>
	</section>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>