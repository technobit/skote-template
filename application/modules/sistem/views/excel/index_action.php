<form method="post" action="<?=$url?>" role="form" class="form-horizontal" id="excel-form" width="80%">
<div id="modal-excel" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1">
				<label for="nama_kolom" class="col-sm-2 col-form-label">Kolom</label>
				<div class="col-sm-10 form-control-sm">
					<h5><b><?=isset($data->var_kode_kolom)? strtoupper($data->var_kode_kolom) : ''?></b></h5>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="var_nama_kolom" class="col-sm-2 col-form-label">Nama</label>
				<div class="col-sm-10">
					<input type="text" class="form-control form-control-sm" id="var_nama_kolom" placeholder="Kolom" name="var_nama_kolom" value="<?=isset($data->var_nama_kolom)? $data->var_nama_kolom : ''?>"/>
				</div>
			</div>
			<input type="hidden" id="var_kode_kolom " name="var_kode_kolom" value="<?=isset($data->var_kode_kolom)? $data->var_kode_kolom : ''?>"/>
		</div>
		<div class="modal-footer">
			<button type="button" data-bs-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>

	$(document).ready(function(){
		$("#excel-form").validate({
			rules: {
				var_nama_kolom: {
					required: true
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
				blockUI(form);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
						unblockUI(form);
						setFormMessage('.form-message', data);
						if(data.stat){
							resetForm('#excel-form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>