<!-- <div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2 content-header-title">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $breadcrumb->title ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <?php
                    if (!empty($breadcrumb->list)) {
                        $last = count($breadcrumb->list);
                        for ($i = 0; $i < $last; $i++) {
                            echo ($i == ($last - 1)) ? '<li class="breadcrumb-item">' . $breadcrumb->list[$i] . '</li>' : '<li class="breadcrumb-item active">' . $breadcrumb->list[$i] . '</li>';
                        }
                    }
                    ?>
                    
                </ol>
            </div>
        </div>
    </div>
</div> -->
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0 font-size-18"><?= $breadcrumb->title ?></h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <?php
                    if (!empty($breadcrumb->list)) {
                        $last = count($breadcrumb->list);
                        for ($i = 0; $i < $last; $i++) {
                            echo ($i == ($last - 1)) ? '<li class="breadcrumb-item"><a href="javascript: void(0);">' . $breadcrumb->list[$i] . '</a></li>' : '<li class="breadcrumb-item active">' . $breadcrumb->list[$i] . '</li>';
                        }
                    }
                    ?>

                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->