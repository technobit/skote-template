<div class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$data->header?></h5>
			<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
            <div class="alert alert-danger">
                  <h5><i class="icon fas fa-ban"></i> <?=$data->title?></h5>
                  <?=$data->message?>
            </div>
		</div>
		<div class="modal-footer">
			<button type="button" data-bs-dismiss="modal" class="btn btn-danger">Keluar</button>
		</div>
	</div>
</div>