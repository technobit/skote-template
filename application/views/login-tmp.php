<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=$page->title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="<?=$this->security->get_csrf_token_name() ?>" content="<?=$this->security->get_csrf_hash() ?>">
    <link href="<?=base_url()?>assets/images/logo.ico" rel="shortcut icon">
    <?php if ($page->cdns) : ?>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css">
    <?php else : ?>
        <link rel="stylesheet" href="<?=base_url() ?>assets/plugins/fontawesome-free/css/all.min.css">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="<?=base_url() ?>assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <?php endif; ?>

    <link rel="stylesheet" href="<?=base_url() ?>assets/dist/css/adminlte.min.css">
    <link rel="stylesheet" href="<?=base_url() ?>assets/dist/css/custom.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b><?=$page->name ?></b></a>
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Sign in to start your session</p>
                <div class="login-message text-center"></div>
                <form action="<?=site_url('login')?>" method="post" id="login-form">
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                        <input id="username" name="username" type="text" class="form-control" placeholder="Username" require  autocomplete="off"/>
                    </div>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        <input id="password" name="password" type="password" class="form-control" placeholder="Password" require />
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <div class="icheck-primary">
                                <input type="checkbox" id="remember">
                                <label for="remember">
                                    Remember Me
                                </label>
                            </div>
                        </div>
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php if ($page->cdns) : ?>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/localization/messages_id.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.min.js"></script>
    <?php else : ?>
        <script src="<?=base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
        <script src="<?=base_url() ?>assets/plugins/moment/moment.min.js"></script>
        <script src="<?=base_url() ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="<?=base_url() ?>assets/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="<?=base_url() ?>assets/plugins/jquery-validation/additional-methods.min.js"></script>
        <script src="<?=base_url() ?>assets/plugins/jquery-validation/localization/messages_id.min.js"></script>
        <script src="<?=base_url() ?>assets/plugins/jquery-validation/jquery.form.min.js"></script>
        <script src="<?=base_url() ?>assets/plugins/jquery-ui/jquery.blockUI.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/daterangepicker/daterangepicker.min.js"></script>
    <?php endif; ?>
    <script src="<?=base_url() ?>assets/dist/js/adminlte.min.js"></script>
    <script src="<?=base_url() ?>assets/dist/js/custom.js"></script>

    <script>
        $(document).ready(function() {
            $("#login-form").validate({
                rules: {
                    username: {
                        required: true,
                        minlength: 4,
                        maxlength: 20
                    },
                    password: {
                        required: true,
                        minlength: 4,
                        maxlength: 20
                    }
                },
                submitHandler: function(form) {
                    $('.login-message').html('');
                    blockUI('body');
                    $(form).ajaxSubmit({
                        dataType:  'json',
                        data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					    success: function(data){
                            setFormMessage('.login-message', data);
                            if(data.stat){
                                setTimeout(() => {
                                    window.location = data.url;
                                }, 1000);
                            }else{
                                unblockUI('body');
                                refreshToken(data);
                            }
                        }
                    });
                },
                validClass: "valid--feedback",
                errorElement: "div", // contain the error msg in a small tag
                errorClass: 'invalid-feedback',
                errorPlacement: erp,
                highlight: hl,
                unhighlight: uhl,
                success: sc
            });

        });
    </script>
</body>

</html>